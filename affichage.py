from tkinter.messagebox import *
from tkinter import *
from tkinter import ttk
from progcesar import chiffrementNonCirculaire
from progcesar import dechiffrementNonCirculaire
from progcesar import chiffrementCirculaire
from progcesar import dechiffrementCirculaire




def updateLabelChiffrement():
		
	if (button_non_circulaire['state'] == DISABLED):
		labelResultat.config(text="Traduction : %s" % chiffrementNonCirculaire(phrase.get(),decalage.get()))
	else:
		labelResultat.config(text="Traduction : %s" % chiffrementCirculaire(phrase.get(),decalage.get()))
def updateLabelDechiffrement():

	if (button_non_circulaire['state'] == DISABLED):
		labelResultat.config(text="Traduction : %s" % dechiffrementNonCirculaire(phrase.get(),decalage.get()))
	else:
		labelResultat.config(text="Traduction : %s" % dechiffrementCirculaire(phrase.get(),decalage.get()))
def updateCirculaire():
	button_circulaire.config(state=DISABLED)
	button_non_circulaire.config(state=NORMAL)
def updateNonCirculaire():
	button_circulaire.config(state=NORMAL)
	button_non_circulaire.config(state=DISABLED)
	

root = Tk()
root.title("Ave Cesar !")
root.geometry('375x200')

root.rowconfigure(10, weight=2)
root.columnconfigure(2, weight=2)

phrase = StringVar()
phrase.set('')

titre = Label(root, text="Mini Projet : Le Chiffre de Cesar")
titre.grid(column=1,row=0)

labelSaisie = Label(root, text="Saississez la phrase a traduire")
labelSaisie.grid(column=1,row=5)

saisie = Entry(root, textvariable=phrase)
saisie.grid(column=1,row=6)

labelResultat = Label(root, text="Traduction : ")
labelResultat.grid(column=1,row = 8)

button_chiffrement = Button(root, text="Chiffrer", command=updateLabelChiffrement)
button_chiffrement.grid(column=0,row=6)

button_dechiffrement = Button(root, text="Dechiffrer", command=updateLabelDechiffrement)
button_dechiffrement.grid(column=2,row=6)

labelCirculaire = Label(root, text="Cliquez sur le type de chiffrement voulu")
labelCirculaire.grid(column=1,row=4)

button_circulaire = Button(root,text="Circulaire" , command=updateCirculaire)
button_circulaire.grid(column=0,row=4)

button_non_circulaire = Button(root,text="Non Circulaire" , command=updateNonCirculaire)
button_non_circulaire.grid(column=2,row=4)

labelDecalage = Label(root,text="Veuillez choisir le décalage voulu :")
labelDecalage.grid(column=1,row=2)
decalage = ttk.Combobox(root, values=[1,2,3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26])
decalage.current(0)              
                                    
                                    
decalage.grid(column=1,row =3)

root.mainloop()

