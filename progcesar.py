import string

def chiffrementNonCirculaire(phrase_a_chiffrer,decalage):
	phrase_chiffree =""
	for char in phrase_a_chiffrer:
		if(char !=" "):
			char = ord(char) + int(decalage)
			char = chr(char)
			phrase_chiffree = phrase_chiffree + char
		else:
			phrase_chiffree = phrase_chiffree + char
	return phrase_chiffree

def dechiffrementNonCirculaire(phrase_a_dechiffrer,decalage):
	phrase_dechiffree =""
	for char in phrase_a_dechiffrer:
		if(char !=" "):
			char = ord(char) - int(decalage)
			char = chr(char)
			phrase_dechiffree = phrase_dechiffree + char
		else:
			phrase_dechiffree = phrase_dechiffree + char
	return phrase_dechiffree


def chiffrementCirculaire(phrase_a_chiffrer,decalage):
	phrase_chiffree =""
	for char in phrase_a_chiffrer:
		if char in string.ascii_lowercase:
			char = ord(char)
			char = (char - 97 + int(decalage) ) %26 + 97
			char = chr(char)
			phrase_chiffree = phrase_chiffree + char
		if char in string.ascii_uppercase:
			char = ord(char)
			char = (char - 65 + int(decalage) ) %26 + 65
			char = chr(char)
			phrase_chiffree = phrase_chiffree + char
	return phrase_chiffree

def dechiffrementCirculaire(phrase_a_dechiffrer,decalage):
	phrase_dechiffree =""
	for char in phrase_a_dechiffrer:
		if char  in string.ascii_lowercase:
			char = ord(char)
			char = (char - 97 - int(decalage) ) %26 + 97
			char = chr(char)
			phrase_dechiffree = phrase_dechiffree + char
		if char in string.ascii_uppercase:
			char = ord(char)
			char = (char - 65 - int(decalage) ) %26 + 65
			char = chr(char)
			phrase_dechiffree = phrase_dechiffree + char
	return phrase_dechiffree

