import unittest

from progcesar import chiffrementNonCirculaire
from progcesar import dechiffrementNonCirculaire
from progcesar import chiffrementCirculaire
from progcesar import dechiffrementCirculaire

class TestChiffreDeCesarFunctions(unittest.TestCase):

	def test_chiffrementnoncirculaire(self):
		self.assertEqual(chiffrementNonCirculaire('HelloWorld',8),'Pmttw_wztl')
		self.assertEqual(chiffrementNonCirculaire('abc',3),'def')

	def test_dechiffrementnoncirculaire(self):
		self.assertEqual(dechiffrementNonCirculaire('Pmttw_wztl )',8),'HelloWorld !')
		self.assertEqual(dechiffrementNonCirculaire('def',3),'abc')

	def test_chiffrementcirculaire(self):
		self.assertEqual(chiffrementCirculaire('HelloWorld',8),'PmttwEwztl')
		self.assertEqual(chiffrementCirculaire('xyz',10),'hij')

	def test_dechiffrementcirculaire(self):
		self.assertEqual(dechiffrementCirculaire('ourge',7),'hnkzx')
		self.assertEqual(dechiffrementCirculaire('abc',10),'qrs')